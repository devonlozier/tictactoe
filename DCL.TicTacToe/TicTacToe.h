#pragma once


class TicTacToe
{
private:

	char m_board [9];
	int m_numTurns;
	char m_playerTurn;
	char m_winner;

	void ClearUnusedCoordinates() 
	{
		for (int i = 0; i < 9; i++)
		{
			if (m_board[i] != 'O' && m_board[i] != 'X')
			{
				m_board[i] = ' ';
			}
		}
	}

public:

	TicTacToe()
	{
		for (int i = 0; i < 9; i++)
		{
			m_board[i] = '1' + i;
		}
		
		m_numTurns = 0;
		m_playerTurn = '1';
		m_winner = ' ';
	}

	void DisplayBoard() 
	{
		std:: cout << "\n  " << m_board[0] << " | " << m_board[1] << " | " << m_board[2] << "\n"
						" -----------\n"
						"  " << m_board[3] << " | " << m_board[4] << " | " << m_board[5] << "\n"
						" -----------\n"
						"  " << m_board[6] << " | " << m_board[7] << " | " << m_board[8] << "\n\n";

		/*  
			"  1 | 2 | 3 \n"
			" -----------\n"
			"  4 | 5 | 6 \n"
			" -----------\n"
			"  7 | 8 | 9 \n\n";
		*/
	}

	bool IsOver()
	{
		// X win logic
		if (m_board[0] == 'X' && m_board[1] == 'X' && m_board[2] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[3] == 'X' && m_board[4] == 'X' && m_board[5] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[6] == 'X' && m_board[7] == 'X' && m_board[8] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[0] == 'X' && m_board[3] == 'X' && m_board[6] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[1] == 'X' && m_board[4] == 'X' && m_board[7] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[2] == 'X' && m_board[5] == 'X' && m_board[8] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[0] == 'X' && m_board[4] == 'X' && m_board[8] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[2] == 'X' && m_board[4] == 'X' && m_board[6] == 'X')
		{
			m_winner = '1';
			ClearUnusedCoordinates();
			return true;
		}

		// O win logic
		if (m_board[0] == 'O' && m_board[1] == 'O' && m_board[2] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[3] == 'O' && m_board[4] == 'O' && m_board[5] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[6] == 'O' && m_board[7] == 'O' && m_board[8] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[0] == 'O' && m_board[3] == 'O' && m_board[6] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[1] == 'O' && m_board[4] == 'O' && m_board[7] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[2] == 'O' && m_board[5] == 'O' && m_board[8] == 'O')
		{
			m_winner = '2'; 
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[0] == 'O' && m_board[4] == 'O' && m_board[8] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}
		else if (m_board[2] == 'O' && m_board[4] == 'O' && m_board[6] == 'O')
		{
			m_winner = '2';
			ClearUnusedCoordinates();
			return true;
		}

		// Neither won: winner = 'False'
		if (m_numTurns == 9)
		{
			m_winner = 'F';
			ClearUnusedCoordinates();
			return true;
		}

		return false;
	}

	char GetPlayerTurn() 
	{
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		if (m_board[position - 1] == 'O' || m_board[position - 1] == 'X')
		{
			return false;
		}
		else if (position < 1 || position > 9)
		{
			return false;
		}

		return true;
	}

	void Move(int position)
	{
		m_numTurns++;

		if (m_playerTurn == '1') 
		{
			m_board[position - 1] = 'X';
			m_playerTurn = '2';
		}
		else
		{
			m_board[position - 1] = 'O';
			m_playerTurn = '1';
		}
	}

	void DisplayResult()
	{
		if (m_winner == '1')
		{
			std::cout << "\nPlayer 1 wins!\n";
		}
		else if (m_winner == '2') 
		{
			std::cout << "\nPlayer 2 wins!\n";
		}
		else if (m_winner == 'F')
		{
			std::cout << "\nTied game!\n";
		}
		else
		{
			std::cout << "\nSomething went horribly wrong!\n";
		}
	}
};